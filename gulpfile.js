// ////////////////////////////////////////////////
// Dependencies
// ////////////////////////////////////////////////

var gulp = require('gulp');
var sass = require('gulp-sass');
var devip = require('dev-ip');
var del = require('del');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var csso = require('postcss-csso');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var fileinclude = require('gulp-file-include');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var gutil = require('gulp-util');

// ////////////////////////////////////////////////
// Configuration
// ////////////////////////////////////////////////

var _CONFIG = {
  host: devip()[0],
  port: 3000,
  paths: {
    dev: {
      root: 'development/assets',
      html: 'development/templates/**/*.html',
      sass: 'development/assets/scss/**/*.scss',
      js: 'development/assets/js'
    },
    pub: {
      root: './public',
      html: 'public',
      css: 'public/css',
      js: 'public/js'
    }
  }
};

// ////////////////////////////////////////////////
// HTML Tasks
// ////////////////////////////////////////////////

gulp.task('fileinclude', function () {
  return gulp.src(_CONFIG.paths.dev.html)
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    })).on('error', gutil.log)
    .pipe(gulp.dest(_CONFIG.paths.pub.html))
    .pipe(browserSync.stream());
});

// ////////////////////////////////////////////////
// SASS Tasks
// ////////////////////////////////////////////////

gulp.task('sass', function () {
  var processors = [
    autoprefixer({browsers: ['> 0.1%', 'last 2 versions', 'Firefox ESR']}),
    csso({restructure: false})
  ];
  return gulp.src(_CONFIG.paths.dev.sass)
    .pipe(sass().on('error', gutil.log))
    .pipe(postcss(processors))
    .pipe(gulp.dest(_CONFIG.paths.pub.css))
    .pipe(browserSync.stream());
});

// ////////////////////////////////////////////////
// JS Tasks
// ////////////////////////////////////////////////

gulp.task('js-libs', function () {
  return gulp.src(_CONFIG.paths.dev.js + '/libs/**/*.js')
    .pipe(gulp.dest(_CONFIG.paths.pub.js + '/libs'))
    .pipe(uglify())
    .pipe(gulp.dest(_CONFIG.paths.pub.js + '/libs'));
});

gulp.task('js-modules', function () {
  return gulp.src([_CONFIG.paths.dev.js + '/scripts.js',
    _CONFIG.paths.dev.js + '/modules/**/*.js'])
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(_CONFIG.paths.pub.js))
    .pipe(rename('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(_CONFIG.paths.pub.js))
    .pipe(browserSync.stream());
});

// ////////////////////////////////////////////////
// Assets Tasks
// ////////////////////////////////////////////////

gulp.task('fonts', function () {
  return gulp.src(_CONFIG.paths.dev.root + '/fonts/**')
    .pipe(gulp.dest(_CONFIG.paths.pub.root + '/fonts'));
});

gulp.task('img', function () {
  return gulp.src(_CONFIG.paths.dev.root + '/img/**')
    .pipe(gulp.dest(_CONFIG.paths.pub.root + '/img'));
});

// ////////////////////////////////////////////////
// Watcher Tasks
// ////////////////////////////////////////////////

gulp.task('fileinclude-watch', ['fileinclude'], reload);
gulp.task('sass-watch', ['sass'], reload);
gulp.task('js-watch', ['js-modules'], reload);

// ////////////////////////////////////////////////
// Default Task
// ////////////////////////////////////////////////

gulp.task('default', ['fileinclude', 'sass', 'js-libs', 'js-modules', 'fonts', 'img'], function () {
  browserSync.init({
    online: true,
    open: false,
    notify: false,
    host: _CONFIG.host,
    port: _CONFIG.port,
    server: {
      baseDir: _CONFIG.paths.pub.root,
      directory: true
    }
  });
  gulp.watch(_CONFIG.paths.dev.html, ['fileinclude-watch']);
  gulp.watch(_CONFIG.paths.dev.sass, ['sass-watch']);
  gulp.watch(_CONFIG.paths.dev.js + '/**/*.js', ['js-watch']);
});