ESTCube
=================================

Project contains all HTML's and frontend stuff connected with ESTCube website.

#### 1. Installing dependencies

Install dependencies by running the following command:

```sh
$ npm install
```

`node_modules` directory will be created during the install.

#### 2. Building the project

Project builds every time when task *gulp default* is in run. To build it first time type in console following commands:

```sh
$ gulp
```

`public` directory will be created during this process.

#### 3. Development

To make changes run gulp default task command:

```sh
$ gulp
```

Remember to edit files only in `development` directory.
Development environment will be created on `http://localhost:3000`