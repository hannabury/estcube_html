window.ESTCUBE = {

  customerAreaMenu: function() {
    $('.customer-area li.menu-item a').on('click', function() {
      $(this).parent().siblings().removeClass('active');
      $(this).parent().addClass('active');

      var target = $(this).attr('href');
      var targetId = target.substring(1, target.length);
      $('.customer-area [class$=-view]').hide();
      $('.customer-area').find('.' + targetId + '-view').fadeIn();

    });
    $('.customer-area li.menu-item.active a').trigger('click');

  },

  menu: function() {
    $('.link-to-register').on('click', function(e) {
      $(this).closest('.navbar').find('.navbar-toggler').click();
    });
  },

  slider: function() {
    $('.slider').slick({
      fade: true,
      dots: true,
      adaptiveHeight: true
    });
  },

  form: function() {
    $('select').select2();
    $('select').on('change.select2', function(e) {
      var value = $(this).val();
      var parent = $(this).closest('.form-group');
      if (value) {
        parent.addClass('selected');
      } else {
        parent.removeClass('selected');
      }
      parent.parent().children('.content').hide();
      parent.parent().children('.content.' + value).show();
    });

    $('input, textarea').on('change', function(){
      if($(this).val() != ''){
        $(this).addClass('filled');
      } else {
        $(this).removeClass('filled');
      }
    });
  },

  init: function() {
    ESTCUBE.customerAreaMenu();
    ESTCUBE.menu();
    ESTCUBE.slider();
    ESTCUBE.form();
  }

};

$(document).ready(function() {
  ESTCUBE.init();
});
